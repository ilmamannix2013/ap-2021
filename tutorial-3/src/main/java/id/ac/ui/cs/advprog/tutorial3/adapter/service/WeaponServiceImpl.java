package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(weaponRepository.findAll().size()<=4){
            for(Spellbook sb : spellbookRepository.findAll()){
                Weapon newSB = new SpellbookAdapter(sb);
                weaponRepository.save(newSB);
            }
            for(Bow bow : bowRepository.findAll()){
                Weapon newBow = new BowAdapter(bow);
                weaponRepository.save(newBow);
            }
        }
        return weaponRepository.findAll();

    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        if(attackType == 1){
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + "(normal attack) : " + weapon.normalAttack());
        }else if(attackType == 2){
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + "(charged attack) : " + weapon.chargedAttack());
        }


    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
