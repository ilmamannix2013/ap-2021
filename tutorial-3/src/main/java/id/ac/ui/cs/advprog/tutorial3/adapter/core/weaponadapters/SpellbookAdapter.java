package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean spellWand;

    // true = bisa melakukan
    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.spellWand = false;

    }

    @Override
    public String normalAttack() {
        this.spellWand = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(!spellWand){
            this.spellWand = true;
            return spellbook.largeSpell();
        }else{
            return "Magic power not enough for large spell!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
