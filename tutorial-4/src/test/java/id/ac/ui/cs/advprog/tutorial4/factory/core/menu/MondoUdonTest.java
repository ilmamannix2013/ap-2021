package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private Menu udon = new MondoUdon("Udon");

    @Test
    public void testGetName() {
        assertEquals("Udon", udon.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(udon.getNoodle() instanceof Udon);
    }

    @Test
    public void testGetMeat() {
        assertTrue(udon.getMeat() instanceof Chicken);
    }

    @Test
    public void testGetTopping() {
        assertTrue(udon.getTopping() instanceof Cheese);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(udon.getFlavor() instanceof Salty);
    }
}
