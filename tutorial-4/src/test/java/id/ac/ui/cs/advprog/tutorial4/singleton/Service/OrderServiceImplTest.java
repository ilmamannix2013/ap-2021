package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.Test;

import java.sql.Driver;

import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceImplTest {
    private OrderServiceImpl orderService = new OrderServiceImpl();

    @Test
    public void testOrderADrink() {
        orderService.orderADrink("tea");
        assertEquals("tea", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderAFood() {
        orderService.orderAFood("pizza");
        assertEquals("pizza", orderService.getFood().getFood());
    }

    @Test
    public void testGetDrinkReturnOrderDrinkInstance() {
        assertNotNull(orderService.getDrink());
    }

    @Test
    public void testGetFoodReturnOrderFoodInstance() {
        assertNotNull(orderService.getFood());
    }

}


