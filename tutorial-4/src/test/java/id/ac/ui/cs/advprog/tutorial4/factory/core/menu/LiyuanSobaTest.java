package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private Menu soba = new LiyuanSoba("Soba");

    @Test
    public void testGetName() {
        assertEquals("Soba", soba.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(soba.getNoodle() instanceof Soba);
    }

    @Test
    public void testGetMeat() {
        assertTrue(soba.getMeat() instanceof Beef);
    }

    @Test
    public void testGetTopping() {
        assertTrue(soba.getTopping() instanceof Mushroom);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(soba.getFlavor() instanceof Sweet);
    }
}
