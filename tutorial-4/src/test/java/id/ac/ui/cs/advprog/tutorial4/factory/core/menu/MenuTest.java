package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;


public class MenuTest {

    public Class<?> menuClass;
    private Menu menu;
    private Menu ramen = new InuzumaRamen("ramen");

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        menu = new MondoUdon("udon");

    }

    @Test
    public void testMenuShouldBeConcreteClass() throws Exception {
        assertFalse(Modifier.
                isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuConstructorInstantiation() throws Exception {
        assertEquals("udon", menu.getName());
        assertTrue(menu.getTopping() instanceof Cheese);
        assertTrue(menu.getNoodle() instanceof Udon);
        assertTrue(menu.getFlavor() instanceof Salty);
        assertTrue(menu.getMeat() instanceof Chicken);
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());

    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());

    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());

    }

    @Test
    public void testMenuHasGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }

}

