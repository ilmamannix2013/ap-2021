package id.ac.ui.cs.advprog.tutorial4.factory.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.*;

public class MenuServiceImplTest {
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuService menuService;

    @BeforeEach
    public void setUp() throws Exception {
        menuRepository = spy(new MenuRepository());
        menuService = new MenuServiceImpl(menuRepository);
    }

    @Test
    public void testMenuServiceHasBeenInitialized() throws Exception {
        menuService = new MenuServiceImpl();
        assertEquals(4, menuRepository.getMenus().size());
    }

    @Test
    public void testInuzumaRamenHasBeenPrepared() throws Exception {
        menuService.getMenus().clear();
        menuService.createMenu("Test", "Ramen");
        Menu inuzumaRamen = menuRepository.getMenus().get(0);
        assertEquals("Test" , inuzumaRamen.getName());
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testLiyuanSobaHasBeenPrepared() throws Exception {
        menuService.getMenus().clear();
        menuService.createMenu("Test", "Soba");
        Menu liyuanSoba = menuRepository.getMenus().get(0);
        assertEquals("Test" , liyuanSoba.getName());
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testMondoUdonHasBeenPrepared() throws Exception {
        menuService.getMenus().clear();
        menuService.createMenu("FoodTest", "Udon");
        Menu output = menuRepository.getMenus().get(0);
        assertEquals("FoodTest" , output.getName());
        assertTrue(output.getNoodle() instanceof Udon);
        assertTrue(output.getMeat() instanceof Chicken);
        assertTrue(output.getTopping() instanceof Cheese);
        assertTrue(output.getFlavor() instanceof Salty);
    }

    @Test
    public void testSnevnezhaShiratakiHasBeenPrepared() throws Exception {
        menuService.getMenus().clear();
        menuService.createMenu("Test", "Shirataki");
        Menu snevnezhaShirataki = menuRepository.getMenus().get(0);
        assertEquals("Test" , snevnezhaShirataki.getName());
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }

}

