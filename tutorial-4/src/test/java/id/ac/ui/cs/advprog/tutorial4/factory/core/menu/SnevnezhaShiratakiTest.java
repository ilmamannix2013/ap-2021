package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private Menu shirataki = new SnevnezhaShirataki("Shirataki");

    @Test
    public void testGetName() {
        assertEquals("Shirataki", shirataki.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(shirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testGetMeat() {
        assertTrue(shirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testGetTopping() {
        assertTrue(shirataki.getTopping() instanceof Flower);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(shirataki.getFlavor() instanceof Umami);
    }
}
