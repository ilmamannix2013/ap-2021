package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InzumanRamenTest {
    private Menu ramen = new InuzumaRamen("Ramen");
    @Test
    public void testGetName() {
        assertEquals("Ramen", ramen.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(ramen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testGetMeat() {
        assertTrue(ramen.getMeat() instanceof Pork);
    }

    @Test
    public void testGetTopping() {
        assertTrue(ramen.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(ramen.getFlavor() instanceof Spicy);
    }
}
