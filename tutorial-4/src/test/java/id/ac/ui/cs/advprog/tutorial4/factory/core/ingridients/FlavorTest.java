package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlavorTest {
    @Test
    public void testSweetDescription() {
        Sweet sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }

    @Test
    public void testUmamiDescription() {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }

    @Test
    public void testSpicyDescription() {
        Spicy spicy = new Spicy();
        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }

    @Test
    public void testSaltyDescription() {
        Salty salty = new Salty();
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}
