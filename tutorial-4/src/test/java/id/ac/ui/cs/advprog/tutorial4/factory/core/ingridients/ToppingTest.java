package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToppingTest {
    @Test
    public void testBoiledEggDescription() {
        BoiledEgg boiledEgg = new BoiledEgg();
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }

    @Test
    public void testCheesDescription() {
        Cheese cheese = new Cheese();
        assertEquals("Adding Shredded Cheese Topping...", cheese.getDescription());
    }

    @Test
    public void testFlowerDescription() {
        Flower flower = new Flower();
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }

    @Test
    public void testMushroomDescription() {
        Mushroom mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...", mushroom.getDescription());
    }
}

