package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderDrinkTest {
    private OrderDrink orderDrink;

    @Test
    public void testOrderDrinkCreatedOnce() {
        OrderDrink orderDrink1 = orderDrink.getInstance();
        OrderDrink orderDrink2 = orderDrink.getInstance();

        assertThat(orderDrink1).isEqualToComparingFieldByField(orderDrink2);
    }

}

