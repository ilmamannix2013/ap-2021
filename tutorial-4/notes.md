## Lazy Instantiation
Lazy Instantiation adalah pembuatan objek yang hanya diperlukan saja ( pertama kali objek dibutuhkan saja).
Keuntungannya yaitu menghemat resource karena objek hanya akan dibuat ketika dibutuhkan saja (tidak boros CPU time).
Kekurangannya yaitu Memungkinkan terjadi multihread (cukup sulit dalam menangani kasus apabila ada 2 thread yang memanggil `getInstance()` ketika objek singletonnya belum dibuat).

## Eager Instantiation
Eager Instantiation adalah pembuatan objek sejak program berjalan. 
Kelebihannya adalah terhindar dari multihread dan dapat digunakan kapanpun.
Kekurangannya adalah sumber daya yang dibutuhkan cukup besar sehingga boros CPU time.