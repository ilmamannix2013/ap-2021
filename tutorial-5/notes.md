**Requirements**
1. Satu matkul dapat menerima banyak mahasiswa.
2. Mahasiswa hanya dapat menjadi asisten di 1 matkul.
3. Mahasiswa dapat mendaftar ke satu matkul, maka langsung diterima. 
4. Mata kuliah yang sudah ada asisten tidak bisa dihapus.
5. Mahasiswa yang sudah menjadi asisten tidak bisa dihapus.
6. Setiap mahasiswa bekerja akan dicatat pada sebuah log.
7. Mahasiswa dapat membuat log
8. Mahasiswa dapat mengedit log dan menghapus log
   <br> Simplenya : Mahasiswa dapat Create,Update,Delete log.
9. Laporan dapat dilihat oleh mahasiswa.
10. Summary total pendapatan.
11. Terdapat data bulan, jam kerja (satuan jam), pembayaran (350 greil/jam).
12. List hal yang dilakukan.

Mahasiswa -> Mata kuliah 
Many to One