package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogBulan;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MahasiswaLogServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private MahasiswaLogServiceImpl mahasiswaLogService;

    private Mahasiswa mahasiswa;

    private Log log;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
        mahasiswaRepository.save(mahasiswa);

        log = new Log();
        SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm");
        log.setStart(formatter.parse("2021-1-30 10:30"));
        log.setEnd(formatter.parse("2021-1-30 12:30"));
        log.setDeskripsi("Asistensi DDP 2 menjelang UTS");
    }

    @Test
    public void testServiceCreateLog() {
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);
        lenient().when(mahasiswaLogService.createLog("1906192052",log)).thenReturn(log);
    }

    @Test
    public void testServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(mahasiswaLogService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = mahasiswaLogService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceDeleteLog() {
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);
        mahasiswaLogService.createLog("1906192052",log);
        mahasiswaLogService.deleteLog("1906192052", log.getIdLog());
        lenient().when(mahasiswaLogService.getLog(log.getIdLog())).thenReturn(null);
        assertNull(mahasiswaLogService.getLog(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateALog() throws ParseException {
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);
        mahasiswaLogService.createLog("1906192052", log);
        Date old_start = log.getStart();

        // Create new log that has updated field
        // in this case : start_on field
        Log new_log = new Log();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        new_log.setStart(formatter.parse("2021-1-30 9:30"));
        new_log.setEnd(formatter.parse("2021-1-30 12:30"));
        new_log.setDeskripsi("Asistensi DDP 2 menjelang UTS");

        when(logRepository.findById(log.getIdLog())).thenReturn(log);
        Log resultLog = mahasiswaLogService.updateALog(log.getIdLog(), new_log);

        assertNotEquals(resultLog.getStart(), old_start);
        assertEquals(resultLog.getDeskripsi(), log.getDeskripsi());
    }

    @Test
    public void testServiceGetLaporanWithNoLog() throws Exception {


        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);

        Laporan lp = mahasiswaLogService.getLaporan("1906192052");

        assertEquals(lp.getTotalPembayaran(), 0);
        assertEquals(lp.getLogBulans().size(), 0);
    }


    @Test
    public void testServiceGetLaporanWithOneLog() throws Exception{

        mahasiswa.setLogs(Arrays.asList(log));
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);

        Laporan lp = mahasiswaLogService.getLaporan("1906192052");

        LogBulan lpb = new LogBulan("January", mahasiswa.getLogs());
        assertEquals(lp.getTotalPembayaran(), lpb.getPembayaran());
    }

    @Test
    public void testServiceGetLaporanWithOneLogAndNegativeTimeDifference() throws Exception{

        SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm");
        log.setStart(formatter.parse("2021-1-30 18:30"));
        log.setEnd(formatter.parse("2021-2-30 1:30"));
        log.setDeskripsi("Asistensi DDP 2 menjelang UTS");

        mahasiswa.setLogs(Arrays.asList(log));
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);

        Laporan lp = mahasiswaLogService.getLaporan("1906192052");

        LogBulan lpb = new LogBulan("January", mahasiswa.getLogs());
        assertEquals(lp.getTotalPembayaran(), lpb.getPembayaran());
    }

    @Test
    public void testServiceGetLaporanWithManyLog() throws Exception{

        // Create second log
        Log log2 = new Log();
        SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm");
        log2.setStart(formatter.parse("2021-1-30 10:30"));
        log2.setEnd(formatter.parse("2021-1-30 13:30"));
        log2.setDeskripsi("Asistensi DDP 1 menjelang UTS");

        List<Log> logs = new ArrayList<>();
        logs.add(log); logs.add(log2);

        mahasiswa.setLogs(logs);
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);

        Laporan lp = mahasiswaLogService.getLaporan("1906192052");

        LogBulan lpb = new LogBulan("January", mahasiswa.getLogs());

        assertEquals(lp.getTotalPembayaran(), lpb.getPembayaran());
    }
}
