package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MahasiswaMataKuliahServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private MahasiswaMataKuliahServiceImpl mahasiswaMataKuliahService;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        mataKuliah = new MataKuliah();
        mataKuliah.setKodeMatkul("CSCM1020");
        mataKuliah.setNama("Advanced Programming");
        mataKuliah.setProdi("Ilmu Komputer");
    }

    @Test
    public void testServiceDaftar() {
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);
        when(mataKuliahRepository.findByKodeMatkul("CSCM1020")).thenReturn(mataKuliah);

        Mahasiswa mhs = mahasiswaMataKuliahService.daftar("CSCM1020", "1906192052");
        assertEquals(mhs, mahasiswa);
    }
}
