package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaLogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MahasiswaLogController.class)
class MahasiswaLogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaLogServiceImpl mahasiswaLogService;

    private Mahasiswa mahasiswa;

    private Log log;

    @BeforeEach
    public void setUp() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        log = new Log(
                formatter.parse("2021-01-03 10:00"),
                formatter.parse("2021-01-03 12:00"),
                "Asistensi Lab DDP 1");
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetListLog() throws Exception{

        Iterable<Log> listLog = Arrays.asList(log);
        when(mahasiswaLogService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(0));
    }

    @Test
    void testControllerCreateLog() throws Exception {
        when(mahasiswaLogService.createLog("1906192052", log)).thenReturn(log);

        mvc.perform(post("/log/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(jsonPath("$.idLog").value(0));
    }

    @Test
    void testControllerUpdateLog() throws Exception {

        log.setDeskripsi("Asistensi Adprog");

        when(mahasiswaLogService.updateALog(0, log)).thenReturn(log);
        mvc.perform(put("/log/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(jsonPath("$.deskripsi").value("Asistensi Adprog"));
    }

    @Test
    void testControllerDeleteLog() throws Exception {

        mahasiswaLogService.createLog("1906192052", log);

        mvc.perform(delete("/log/1906192052/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }

    @Test
    void testControllerGetLaporan() throws Exception {

        mahasiswaLogService.createLog("1906192052", log);

        mahasiswa.setLogs(Arrays.asList(log));

        when(mahasiswaLogService.getLaporan("1906192052")).thenReturn(new Laporan(mahasiswa));

        mvc.perform(get("/log/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.totalPembayaran").value(2*350));
    }
}
