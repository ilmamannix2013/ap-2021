package csui.advpro2021.tais.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;

import java.text.ParseException;

public interface MahasiswaLogService {
    Log createLog(String npm, Log log);

    Log updateALog(int id, Log log) throws ParseException;

    Log getLog(int id);

    Iterable<Log> getListLog();

    void deleteLog(String npm, int id);

    Laporan getLaporan(String npm) ;
}
