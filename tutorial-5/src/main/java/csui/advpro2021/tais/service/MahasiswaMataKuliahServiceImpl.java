package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaMataKuliahServiceImpl implements MahasiswaMataKuliahService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Mahasiswa daftar(String matkul, String mahasiswa) {
        Mahasiswa mhs = this.mahasiswaRepository.findByNpm(mahasiswa);
        MataKuliah mataKuliah = this.mataKuliahRepository.findByKodeMatkul(matkul);

        mhs.setMatkul(mataKuliah);

        // save di repo mahasiswa dan matkul karena cascade type 'persist'
        this.mahasiswaRepository.save(mhs);
        this.mataKuliahRepository.save(mataKuliah);
        return mhs;
    }
}
