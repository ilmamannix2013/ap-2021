package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class MahasiswaLogServiceImpl implements MahasiswaLogService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mhs = this.mahasiswaRepository.findByNpm(npm);

        log.setMahasiswa(mhs);

        // save log, cascade type 'all'
        this.logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(int id) {
        return logRepository.findById(id);
    }

    @Override
    public Iterable<Log> getListLog() {
        return this.logRepository.findAll();
    }

    @Override
    public Log updateALog(int id, Log log) throws ParseException {
        Log oldLog = this.logRepository.findById(id);
        oldLog.setStart(log.getStart());
        oldLog.setEnd(log.getEnd());
        oldLog.setDeskripsi(log.getDeskripsi());
        this.logRepository.save(oldLog);
        return oldLog;
    }

    @Override
    public void deleteLog(String npm, int id) {
        Mahasiswa mhs = this.mahasiswaRepository.findByNpm(npm);
        mhs.getLogs().remove(this.logRepository.findById(id));
        this.logRepository.deleteById(id);
    }

    @Override
    public Laporan getLaporan(String npm) {
        Mahasiswa mhs = this.mahasiswaRepository.findByNpm(npm);

        return new Laporan(mhs);
    }
}
