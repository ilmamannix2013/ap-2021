package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;

public interface MahasiswaMataKuliahService {
    Mahasiswa daftar(String matkul, String mahasiswa);
}
