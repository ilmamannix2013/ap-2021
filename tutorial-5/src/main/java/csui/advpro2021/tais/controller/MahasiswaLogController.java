package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.MahasiswaLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/log")
public class MahasiswaLogController {

    @Autowired
    private MahasiswaLogService mahasiswaLogService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListMahasiswa() {
        return ResponseEntity.ok(mahasiswaLogService.getListLog());
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLaporan(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(mahasiswaLogService.getLaporan(npm));
    }


    @PostMapping(path = "/{npm}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postMahasiswa(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(mahasiswaLogService.createLog(npm, log));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateMataKuliah(@PathVariable(value = "id") int id, @RequestBody Log log) throws ParseException {
        return ResponseEntity.ok(mahasiswaLogService.updateALog(id, log));
    }

    @DeleteMapping(path = "/{npm}/{id}", produces = {"application/json"})
    public ResponseEntity deleteMataKuliah(@PathVariable(value = "npm") String npm, @PathVariable(value = "id") int idlog) {
        mahasiswaLogService.deleteLog(npm, idlog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
