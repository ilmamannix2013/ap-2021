package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaMataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/asistensi")
public class MahasiswaMataKuliahController {

    @Autowired
    private MahasiswaMataKuliahService mahasiswaMataKuliahService;


    @GetMapping(path = "/daftar/{npm}/{kode}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMahasiswa(@PathVariable(value = "kode") String kode, @PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(mahasiswaMataKuliahService.daftar(kode, npm));
    }
}
