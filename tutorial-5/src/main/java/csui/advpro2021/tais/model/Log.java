package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.*;
import java.util.Date;
import java.util.TimeZone;

@Entity
@Table(name="log")
@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idLog")
public class Log {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "start_on")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date start;

    @Column(name = "end_on")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    @Column(name = "deskripsi", columnDefinition = "TEXT")
    private String deskripsi;

    public void setMahasiswa(Mahasiswa mahasiswa) {
        mahasiswa.getLogs().add(this);
    }

    public void setStart(Date start) {

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        this.start = start;
    }

    public void setEnd(Date end) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        this.end = end;
    }

    public Log(Date start, Date end, String deskripsi) {
        this.start = start;
        this.end = end;
        this.deskripsi = deskripsi;
    }
}