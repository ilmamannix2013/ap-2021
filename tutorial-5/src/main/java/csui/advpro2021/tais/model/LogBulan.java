package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Calendar;
import java.util.List;

@Entity
@Data
@NoArgsConstructor

public class LogBulan {
    @Id
    private String month;
    private int jamKerja;
    private int pembayaran;


    public LogBulan(String month, List<Log> logs) {
        this.month = month;
        Calendar cal = Calendar.getInstance();
        for (Log log: logs
        ) {
            cal.setTime(log.getStart());
            int start = cal.get(Calendar.HOUR_OF_DAY);

            cal.setTime(log.getEnd());
            int end = cal.get(Calendar.HOUR_OF_DAY);

            int difference = end - start;
            this.jamKerja += difference > 0 ? difference : 12 + difference;
        }
        // 350 greil/jam
        this.pembayaran = this.jamKerja * 350;
    }

    // Method

    public int getJamKerja() {
        return jamKerja;
    }

    public int getPembayaran() {
        return pembayaran;
    }

    public String getMonth() {
        return month;
    }

}
