package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "mahasiswa")
@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "npm")
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    //add
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "matkul_id", foreignKey = @ForeignKey(name="mahasiswa_matkul_id_fkey"))
    private MataKuliah matkul;

    // Buat relasi 0ne to Many di Mahasiswa
    @OneToMany(cascade = CascadeType.ALL)
    private List<Log> logs = new ArrayList<>();

    //add
    public void setMatkul(MataKuliah matkul) {
        this.matkul = matkul;
        matkul.getMahasiswas().add(this);
    }

    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
    }
}
