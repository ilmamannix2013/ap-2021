package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.text.DateFormatSymbols;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
public class Laporan {

    @Id
    private int totalPembayaran;

    @OneToMany
    private List<LogBulan> logBulans = new ArrayList<>();

    public Laporan(Mahasiswa mahasiswa) {
        List<Log> logs = mahasiswa.getLogs();

        if (logs.isEmpty()) {
            this.totalPembayaran = 0;
        } else {
            Calendar cal = Calendar.getInstance();
            // mapping bulan ke list dalam log
            // untuk mendapatkan total waktu perbulan
            Map<String, List<Log>> bulanLogsMap = getBulanLogMap(logs, cal);

            for (String monthName: bulanLogsMap.keySet()
            ) {
                // Class ini akan menampilkan log per bulannya
                // dan menghitung jumlah pendapatan
                LogBulan lpb = new LogBulan(monthName, bulanLogsMap.get(monthName));
                logBulans.add(lpb);

                // get total pendapatan dari semua bulan
                this.totalPembayaran += lpb.getPembayaran();
            }
        }


    }

    private Map<String, List<Log>> getBulanLogMap(List<Log> logs, Calendar cal) {
        Map<String, List<Log>> bulanLogsMap = new HashMap<>();
        for (Log log: logs
        ) {
            // get nama bulan (month in int -> month name in String)
            cal.setTime(log.getStart());
            int month = cal.get(Calendar.MONTH);
            String monthName = getMonth(month);

            // create list log
            List<Log> monthlyLogs = new ArrayList<>();
            monthlyLogs.add(log);
            if (bulanLogsMap.putIfAbsent(monthName, monthlyLogs) != null) {
                // kalau udah ada, langsung add
                bulanLogsMap.get(monthName).add(log);
            }

        }
        return bulanLogsMap;
    }

    private String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }
}
