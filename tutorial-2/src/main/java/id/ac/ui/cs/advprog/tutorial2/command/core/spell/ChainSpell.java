package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> aSpell;

    public ChainSpell(ArrayList<Spell> aSpell){
        this.aSpell = aSpell;
    }

    @Override
    public void cast() {
        for(Spell spell : aSpell){
            spell.cast();
        }

    }

    @Override
    public void undo() {
        for (int i = aSpell.size() - 1; i >= 0; i--) {
            aSpell.get(i).undo();
        }


    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
