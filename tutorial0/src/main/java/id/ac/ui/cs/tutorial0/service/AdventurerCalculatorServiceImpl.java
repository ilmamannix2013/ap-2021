package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {
    int fPower;
    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            fPower = rawAge*2000;
            return rawAge*2000;
        } else if (rawAge <50) {
            fPower = rawAge*2250;
            return rawAge*2250;
        } else {
            fPower = rawAge*5000;
            return rawAge*5000;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    @Override
    public String classPower() {
        if (fPower > 0 && fPower < 20000) {
            return "C class";
        } else if (fPower > 20000 && fPower < 100000) {
            return "B class";
        } else if (fPower > 100000) {
            return "A class";
        } else {
            return "Diluar range";
        }
    }
}
